# Attention RestNet For Place Classification

<div>
<h1>Loss Value</h1>
</div>

![Alt text](result/loss.png?raw=true "Loss Value")

<div>
<h1>Model Accuracy</h1>
</div>

![Alt text](result/accuracy.png?raw=true "Accuracy")

<div>
<h1>Model F1-Score</h1>
</div>

![Alt text](result/f1score.png?raw=true "F1-Score")
