import os
from random import shuffle
from typing import List

import cv2
import numpy
import numpy as np
from skimage.io import imread
from skimage.transform import resize

TRAIN_PATH = 'dataset/DataSet_SEQUENCE_1'
VALID_PATH = 'dataset/DataSet_SEQUENCE_2'
TEST_PATH = 'dataset/DataSet_SEQUENCE_3'

MAPPINGS = {
    "F104_RGB": 0,
    "F102_RGB": 1,
    "F107_RGB": 2,
    "D7_RGB": 3,
    "D3A_RGB": 4,
    "F105_RGB": 5,
    "Corridor2_RGB": 6,
    "Corridor3_RGB": 7,
    "Corridor1_RGB": 8
}


def prepare_train_data():
    return prepare_dataset(TRAIN_PATH)


def prepare_valid_data():
    return prepare_dataset(VALID_PATH)


def prepare_test_data():
    return prepare_dataset(TEST_PATH)


def prepare_dataset(path: str):
    img_paths: List[str] = []

    for path, sub_dirs, files in os.walk(path):
        for name in files:
            img = os.path.join(path, name)
            img_paths.append(img)

    shuffle(img_paths)
    labels = list(map(lambda x: encode_label(x), img_paths))

    # x_train, x_test, y_train, y_test = train_test_split(img_paths, labels, test_size=0.2)
    return img_paths, labels


def encode_label(path):
    label_str = path.split('/')[-2]
    label_int = MAPPINGS.get(label_str)
    return label_int


def batch_iterator(x, y):
    batch_size = 128

    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < len(x):
            limit = min(batch_end, len(x))
            x_batch = read_images(x[batch_start:limit])
            y_batch = y[batch_start:limit]
            y_batch = numpy.append(y_batch, y_batch, axis=0)

            yield x_batch, y_batch

            batch_start += batch_size
            batch_end += batch_size


def read_images(paths):
    images = [read_image(x) for x in paths]
    images, flipped = list(map(lambda x: x[0], images)), list(map(lambda x: x[1], images))
    images = images + flipped
    return np.asarray(images)


def read_image(path):
    image = imread(path)
    image = resize(image, (96, 128), anti_aliasing=True)
    norm_img = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    flip_img = cv2.flip(norm_img, 1)
    return norm_img, flip_img
