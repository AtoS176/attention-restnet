from keras.layers import AveragePooling2D, Dropout
from keras.layers import Conv2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.models import Model
from keras.regularizers import l2

from project.model.Attention_Block import Attention_Block
from project.model.Residual_Unit import Residual_Unit


def AttentionRestNet(shape, in_channel, kernel_size, n_classes, dropout=0.3, regularization=0.01):
    # Stage 1
    input_data = Input(shape=shape)
    x = Conv2D(in_channel, kernel_size=kernel_size, padding='same')(input_data)
    x = MaxPooling2D(pool_size=4, padding='same')(x)

    # Stage 2
    out_channel = in_channel * 4
    x = Residual_Unit(x, in_channel, out_channel)
    x = Attention_Block(x, skip=2)

    # Stage 3
    in_channel = out_channel // 2
    out_channel = in_channel * 4
    x = Residual_Unit(x, in_channel, out_channel, stride=2)
    x = Attention_Block(x, skip=1)

    # Stage 4
    in_channel = out_channel // 2
    out_channel = in_channel * 4
    x = Residual_Unit(x, in_channel, out_channel, stride=2)
    x = Attention_Block(x, skip=1)

    # Stage 5
    in_channel = out_channel // 2
    out_channel = in_channel * 4
    x = Residual_Unit(x, in_channel, out_channel, stride=1)

    # Stage 6
    x = AveragePooling2D(pool_size=(6, 8))(x)
    x = Flatten()(x)
    x = Dropout(dropout)(x)

    output = Dense(n_classes, kernel_regularizer=l2(regularization), activation='softmax')(x)
    model = Model(input_data, output)

    return model
