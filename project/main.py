import datetime
import time

import tensorflow as tf
from keras.callbacks import TensorBoard
from keras.losses import CategoricalCrossentropy
from keras.utils.np_utils import to_categorical

from custom_metrics import *
from data_provider import *
from model.AttentionResNet56 import AttentionRestNet


def train(epc=50):
    x_train, y_train = prepare_train_data()
    x_valid, y_valid = prepare_valid_data()

    y_train = to_categorical(y_train, 9)
    y_valid = to_categorical(y_valid, 9)

    cross_entropy = CategoricalCrossentropy()
    sgd = tf.keras.optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True)

    print('Training AttentionResNet56')
    model = AttentionRestNet(shape=(96, 128, 3), in_channel=32, kernel_size=3, n_classes=9, dropout=0.5)
    model.compile(loss=cross_entropy, optimizer=sgd, metrics=['accuracy', f1])

    batch_size = 128
    start = time.time()

    log_dir = 'Logs/' + datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

    train_gen = batch_iterator(x_train, y_train)
    valid_gen = batch_iterator(x_valid, y_valid)

    history = model.fit(train_gen,
                        validation_data=valid_gen,
                        steps_per_epoch=len(x_train) // batch_size,
                        validation_steps=len(x_valid) // batch_size,
                        epochs=epc,
                        callbacks=[tensorboard_callback])

    end = time.time()
    print("Time taken by above cell is {}.".format((end - start) / 60))

    return model


if __name__ == '__main__':
    model = train()
    model.save('Model.h5')
